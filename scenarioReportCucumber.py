#!/usr/bin/env python
# coding: utf-8

import glob
import os
import re
import sys
import argparse
import json
from pprint import pprint

OUTPUT_HEADER = "Application; Test Suite; Test Case; Overview; Preconditions; Pass Criteria; Result\n"

def get_root_directory(project_path):
    root_directory = "./"
    if project_path is not None:
        root_directory = project_path
        if not root_directory.endswith("/"):
            root_directory += "/"
    return root_directory

def get_project_applications(project_path):
    root_directory = get_root_directory(project_path)
    def project_path(subpath):
        return  root_directory + subpath

    return [
        ("Operation Portal", project_path("mudp/operation/webapp/src/cucumber/resources/features/")),
        ("User Portal", project_path("mudp/user/webapp/src/cucumber/resources/features/")),
        ("Dealer Portal", project_path("mudp/user/dealer-webapp/src/cucumber/resources/features/")),
        ("Map Downloader", project_path("mudp-downloader/qa/")),
    ]

def generate_json(input_folder, tags):
    if tags is None:
        print("\nGenerating json for " + input_folder)
        command = "cucumber -d -i -f json -o 1.json " + input_folder
    else:
        print("\nGenerating json for " + input_folder + " with tags: " + tags)
        command = "cucumber -d -i"
        for tag in tags.split():
            command += " -t " + tag
        command += " -f json -o 1.json " + input_folder
    print "Executing command: " + command
    os.system(command)

def read_features():
    data = json.load(open('1.json'))
    return data

def scenario_as_string(application, feature_name, scenario, overrided_scenario_name = None):
    description_parts = re.split("(?:!Overview:)|(?:!Preconditions:)|(?:!Pass Criteria:)", scenario["description"], flags=re.IGNORECASE)
    
    try:
        overview = description_parts[1].strip().replace("\n", " ").replace(";", ".")
        if "!Preconditions" in scenario["description"]:
            preconditions = description_parts[2].strip().replace("\n", " ").replace(";", ".")
            pass_criteria = description_parts[3].strip().replace("\n", " ").replace(";", ".")
        else:
            preconditions = "- none"
            pass_criteria = description_parts[2].strip().replace("\n", " ").replace(";", ".")

        return ";".join([
            application,
            feature_name,
            scenario["name"] if overrided_scenario_name is None else overrided_scenario_name,
            overview,
            preconditions,
            pass_criteria,
            "no run"]) + "\n"

    except IndexError:
        print "Wrong description: one of the mandatory parts is absent for \"" + scenario["name"] + "\" scenario: "
        print(scenario["description"])
        print "\n"

def scenario_outline_as_string(application, feature_name, scenario):
    m = re.search("<(.*?)>", scenario["name"])
    if m is None:
        print "Warning: scenario outline name is not parametrized for " + scenario["name"]
        return scenario_as_string(application, feature_name, scenario)
    else:
        column = 0
        example = scenario["examples"][0]
        for i in range(0, len(example["rows"][0]["cells"])): 
            if example["rows"][0]["cells"][i] == m.group(1):
                column = i

        scenario_outline_string = ""
        for i in range(1, len(example["rows"])): 
            new_scenario_name = scenario["name"].replace(m.group(0), example["rows"][i]["cells"][column])
            scenario_outline_string += scenario_as_string(application, feature_name, scenario, overrided_scenario_name = new_scenario_name)
        
        return scenario_outline_string

def usage():
    print 'scenarioReportCucumber.py [-i|input <inputfile>] -o <outputfile>'

def main(argv):
    parser = argparse.ArgumentParser(description='Generates test cases reports based on the cucumber files.', epilog='Developed by Leila Miniailo, Harman Connected Services, 2017')
    parser.add_argument('-i', '--project_path', default='./', help='specifies project root path')
    parser.add_argument('-o', '--output_file', default='./test_report.txt', help='specifies destination and name of the output report file')
    parser.add_argument('-t', '--tags', help='specifies tags of the scenarios to be taken into account')
    args = parser.parse_args()

    print "Saving report to " + args.output_file
    output_file = open(args.output_file, 'w')
    output_file.write(OUTPUT_HEADER)

    for application, path in get_project_applications(args.project_path):
        generate_json(path, args.tags)
        features_json = read_features()
        for feature in features_json:
            for feature_element in feature["elements"]:
                if feature_element["type"] == "scenario":
                    string_to_write = scenario_as_string(application, feature["name"], feature_element)
                    output_file.write(string_to_write.encode("utf8"))
                if feature_element["type"] == "scenario_outline":
                    string_to_write = scenario_outline_as_string(application, feature["name"], feature_element)
                    output_file.write(string_to_write.encode("utf8"))
    
    output_file.close()
    os.remove('1.json')

main(sys.argv)