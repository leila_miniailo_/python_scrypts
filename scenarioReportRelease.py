#!/usr/bin/env python
# coding: utf-8

import glob
import os
import re
import sys
import argparse
import json
import xlsxwriter
from pprint import pprint

OUTPUT_HEADER = "Application; Test Suite; Test Case; Result"

reload(sys)  
sys.setdefaultencoding('utf8')

def get_root_directory(project_path):
    root_directory = "./"
    if project_path is not None:
        root_directory = project_path
        if not root_directory.endswith("/"):
            root_directory += "/"
    return root_directory

def get_project_applications(project_path):
    root_directory = get_root_directory(project_path)
    def project_path(subpath):
        return  root_directory + subpath

    return [
        ("Operation Portal", project_path("mudp/operation/webapp/src/cucumber/resources/features/")),
        ("User Portal", project_path("mudp/user/webapp/src/cucumber/resources/features/")),
        ("Dealer Portal", project_path("mudp/user/dealer-webapp/src/cucumber/resources/features/")),
        ("Map Downloader", project_path("mudp-downloader/qa/")),
    ]

def generate_json(input_folder, tag):
    print("\nGenerating json for " + input_folder + " with tag: " + tag)
    command = "cucumber -d -i -t " + tag + " -f json -o 1.json " + input_folder
    print "Executing command: " + command
    os.system(command)

def read_features():
    data = json.load(open('1.json'))
    return data

def scenario_as_string(application, feature_name, scenario, overrided_scenario_name = None):

    return ";".join([
        application,
        feature_name,
        scenario["name"] if overrided_scenario_name is None else overrided_scenario_name,
        "no run",
        "" if overrided_scenario_name is None else "@@@",
        ""
        ])

def scenario_outline_as_string(application, feature_name, scenario):
    m = re.search("<(.*?)>", scenario["name"])
    if m is None:
        print "Warning: scenario outline name is not parametrized for " + scenario["name"]
        return scenario_as_string(application, feature_name, scenario)
    else:
        column = 0
        example = scenario["examples"][0]
        for i in range(0, len(example["rows"][0]["cells"])): 
            if example["rows"][0]["cells"][i] == m.group(1):
                column = i

        scenario_outline_string = ""
        for i in range(1, len(example["rows"])): 
            new_scenario_name = scenario["name"].replace(m.group(0), example["rows"][i]["cells"][column])
            scenario_outline_string += scenario_as_string(application, feature_name, scenario, overrided_scenario_name = new_scenario_name)      
        return scenario_outline_string

def usage():
    print 'scenarioReportCucumber.py [-i|input <inputfile>] -o <outputfile>'

def main(argv):
    parser = argparse.ArgumentParser(description='Generates test reports based on the cucumber files. It is used for Release', epilog='Developed by Leila Miniailo, Harman Connected Services, 2017')
    parser.add_argument('-i', '--project_path', default='./', help='specifies project root path')
    parser.add_argument('-o', '--output_file', default='./testReport.xlsx', help='specifies destination and name of the output report file')
    args = parser.parse_args()
    
    workbook = xlsxwriter.Workbook('testReport.xlsx')
    tags_to_find = ["@smoke", "@auto -t ~@smoke -t ~@i18n", "~@auto -t ~@smoke -t ~@i18n -t ~@downloader", "@i18n -t @auto", "@i18n -t ~@auto", "@cross-browser", "@portability", "@installability"]
    sheets_name = ["Smokes", "Automated system tests", "System tests", "Automated localization tests", "Localization", "Cross-browser tests", "Portability Map Downloader", "Installability Map Downloader"]
    worksheet = workbook.add_worksheet
    for tag in tags_to_find:
        worksheet = workbook.add_worksheet(sheets_name[tags_to_find.index(tag)])
        col = 0
        for column_name in OUTPUT_HEADER.split(';'):
            worksheet.write(0, col, column_name)
            col += 1
        row = 0
        for application, path in get_project_applications(args.project_path):
            generate_json(path, tag)
            features_json = read_features()
            for feature in features_json:
                for feature_element in feature["elements"]:
                    col = 0
                    if feature_element["type"] == "scenario":
                        string_to_write = scenario_as_string(application, feature["name"], feature_element)
                        strings = string_to_write.split(';')
                        for scenario_item in strings:
                            worksheet.write(row, col, scenario_item.encode("utf8"))
                            col += 1
                    if feature_element["type"] == "scenario_outline":
                        string_to_write = scenario_outline_as_string(application, feature["name"], feature_element)
                        strings = string_to_write.split(';')
                        for scenario_item in strings:
                            if scenario_item == "@@@":
                                col = 0
                                continue
                            scenario_item.replace('@@@', '')
                            worksheet.write(row, col, scenario_item.encode("utf8"))
                            col += 1    
                    row = row + 1
    os.remove('1.json')
    workbook.close()
main(sys.argv)